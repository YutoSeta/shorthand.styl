# shorthand.styl
「shorthand.styl」は既存のCSSプロパティを、意味を損ねずに、無駄な記述を極力減らし、
より効率的にスタイルシートを記述できるように書かれたstylusライブラリです。
例えば、こんな風にスタイルを記述できるようになります。

```
.Container{
  a1: 0 0; //position: absolute; top: 0; left: 0;
  mx: auto; //margin-left: auto; marign-right: auto;
  width: 1200px;
  height: auto;
  pt: 60px; //padding-top: 40px
  pb: 40px; //padding-bottom: 40px
  display: flex;
  jc: center; //justify-content: center;
  ai: center; //align-items: center;
}
``` 

# Dependency
Stylus

# Setup
stylusファイルにshorthand.stylをimportしたらセットアップ完了です。

# Usage
shorthand.stylに定義されたプロパティを指定して使います。

```
.Container{
  a1: 0 0; //position: absolute; top: 0; left: 0;
  mx: auto; //margin-left: auto; marign-right: auto;
  width: 1200px;
  height: auto;
  pt: 60px; //padding-top: 40px
  pb: 40px; //padding-bottom: 40px
  display: flex;
  jc: center; //justify-content: center;
  ai: center; //align-items: center;
}
``` 

# Authors
YutoSeta
Twitter: @YutoSeta
